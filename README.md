
Mektildis Website
============

Is a backend server for managing Mektildis Website powered by mezzanine

Features
--------
- Pages
- Blogs
- Medias


Dependencies
------------
Install python-dev, pip, nodejs, bower and virtualenvwrapper


```
#!bash

$ sudo apt-get install python-dev python-pip nodejs
$ sudo npm install -g bower
$ sudo apt-get install virtualenvwrapper
```


Installation
------------
Install mukulu website by running:

```
#!bash

$ git clone git@bitbucket.org:mukulu/mektildis.git
$ cd mektildis
$ mkvirtualenv mektildis --no-site-packages
$ pip install -r requirements.txt
$ python manage.py createdb
```

# TODO
export DJANGO_SETTINGS_MODULE=mukulu.settings.local

Run server
```
#!bash

$ python manage.py runserver
```
# Deploying to product server

Start gunicorn inside  project dir
```
#!bash
workon mukulu
$ gunicorn_django -D -c gunicorn.conf.py
```
Setup nginx with below configurations for reverse proxying gunicorn
```
proxy_cache_path  /var/cache/nginx  keys_zone=mukulu:250m  inactive=1d;
proxy_redirect    off;
proxy_set_header  Host               $host;
proxy_set_header  X-Real-IP          $remote_addr;
proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;

server {

        listen 80;
        #Assumes nginx should only serve the app for these domains only
        server_name www.firstdomain.com seconddomain.com;
        root /path/to/project/directory;

        sendfile        on;
        include       mime.types;
        default_type  application/octet-stream;
        keepalive_timeout  390;

        index index.html index.htm;

        error_log /var/log/nginx/error.log;
        access_log /var/log/nginx/access.log;
 
       
        location /static {
				#Assumes static folder is in project root
                root /path/to/project/directory;
        }
        location / {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;

                proxy_redirect off;
                #Assumes gunicorn is running on port 8888
                proxy_pass http://127.0.0.1:8888;
        }

}
```

Generate static files to static folder for serving nginx
```
python manage.py collectstatic
```

Once the server deployed, in admin interface,
create home page with slug "/". Home page is set to redirect there.


Contribute
----------

- Issue Tracker: https://bitbucket.org/mukulu/mukulu/issues
- Source Code: https://bitbucket.org/mukulu/mukulu

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: allen.machary@gmail.com

License
-------
The project is licensed under the GNU GPL license.

Authors
-------
John Francis Mukulu <john.f.mukulu@gmail.com>
